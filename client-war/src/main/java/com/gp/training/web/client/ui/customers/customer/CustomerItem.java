package com.gp.training.web.client.ui.customers.customer;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class CustomerItem extends Composite {

	private static CustomerItemUiBinder uiBinder = GWT.create(CustomerItemUiBinder.class);

	interface CustomerItemUiBinder extends UiBinder<Widget, CustomerItem> {
	}

	public CustomerItem() {
		initWidget(uiBinder.createAndBindUi(this));
	}

}
