package com.gp.training.bizlogic.client.resource;

import com.gp.training.bizlogic.api.model.BookingDTO;

public interface BookingService {
	
	public BookingDTO createBooking(BookingDTO booking);
}
