package com.gp.training.bizlogic.dao;

import java.util.List;

import com.gp.training.bizlogic.api.model.OfferDTO;
import com.gp.training.bizlogic.params.AvailSearchParams;

public interface AvailabilityDao {
	
	public List<OfferDTO> findOffers(AvailSearchParams params);
}
