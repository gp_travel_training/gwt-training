package com.gp.training.bizlogic.dao;

import org.springframework.stereotype.Repository;

import com.gp.training.bizlogic.domain.Booking;

@Repository
public class BookingDaoImpl extends GenericDaoImpl<Booking, Long> implements BookingDao {
	
	public BookingDaoImpl(){
		super(Booking.class);
	}
	
	@Override
	public Booking create(Booking booking) {
		//TODO: implement
		return booking;
	}
	
}
