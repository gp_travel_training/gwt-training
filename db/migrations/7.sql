-- Таблица букингов
create table booking (
	id int(11) primary key auto_increment,
	offer_id int(11) not null,
	foreign key (offer_id) references offer(id)
) engine=InnoDB default charset=utf8;