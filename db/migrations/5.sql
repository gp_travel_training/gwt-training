-- Таблица отелей
create table hotel(
	id int(11) primary key  auto_increment, 
	city_id int(11) not null,
	code varchar(10) not null, 
    name varchar(64) not null, 
	foreign key (city_id) references city(id)
) engine=InnoDB default charset=utf8;

insert into hotel(city_id, code, name)
	values (1, "BHM", "Belarus Hotel"), (1, "VHM", "Victoria Hotel"), (1, "PHM", "President Hotel"),
		   (2, "SHG", "Semashko Hotel"), (2, "NG", "Neman"),
		   (3, "HDLP", "Hotel Du Louvre"), (3, "NPP", "Napoleon Paris"),
		   (4, "BRN", "Beau Rivage"), (4, "HNN", "Hotel Negresco");

-- Таблица предложений
create table offer(
	id int(11) primary key  auto_increment, 
	hotel_id int(11) not null, 
	room_type_id int(11) not null,
	foreign key (hotel_id) references hotel(id),
	foreign key (room_type_id) references room_type(id)
) engine=InnoDB default charset=utf8;

insert into offer(hotel_id, room_type_id)
	select h.id, rt.id  from hotel h join room_type rt; 